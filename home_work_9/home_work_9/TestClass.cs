﻿using System;
using System.Collections.Generic;
using System.Text;

namespace home_work_9
{
    public class TestClass
    {
        public string FieldStr { get; set; }
        public int FieldInt { get; set; }

        public TestClass(string s, int i)
        {
            this.FieldInt = i;
            this.FieldStr = s;
        }
    }
}
