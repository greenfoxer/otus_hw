﻿using System;
using System.Collections.Generic;
using System.Text;

namespace home_work_9
{
    /// <summary>
    /// Задание 1. Функция расширения для класса List, возвращающая аксимальный элемент.
    /// </summary>
    public static class ExtensionMethod
    {
        public static T GetMax<T>(this List<T> list, Func<T, T, bool> getMax) where T : class
        {
            T current = list[0];
            foreach (T item in list)
                if (getMax(item, current))
                    current = item;
            return current;
        }
    }
}
