﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.ComponentModel;
using System.IO;

namespace home_work_9
{
    /// <summary>
    /// Задание 2. Класс-поисковик файлов. Выдает событие с
    /// </summary>
    public class FileFinder
    {
        //protected internal delegate void FindFoundEventHandler(object sender, FileArgs s);
        //protected internal event FindFoundEventHandler FileFound;

        protected internal event EventHandler<FileArgs> FileFound;

        CancelEventArgs cancelArgs = new CancelEventArgs();

        public string Path { get; set; }
       // public bool IsStop { get; internal set; }

        public FileFinder(string path)
        {
            this.Path = path;
        }
        public void Find(string stopWord)
        {
            foreach (var file in Directory.GetFiles(Path, "*", SearchOption.AllDirectories))
            {
                FileFound?.Invoke(this, new FileArgs(file));
                var current = new System.IO.FileInfo(file);
                //Задание 4. Отмена поиска при достижении определенного файла, в данном случае stopWord = flag.txt
                if (string.Compare(stopWord, current.Name) == 0)
                {
                    FileFound = null;
                    break;
                }
            }
        }

    }
}
