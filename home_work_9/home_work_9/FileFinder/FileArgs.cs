﻿using System;
using System.Collections.Generic;
using System.Text;

namespace home_work_9
{
    public class FileArgs : EventArgs
    {
        public string Name { get; private set; }

        public FileArgs(string n)
        {
            this.Name = n;
        }
        public override string ToString()
        {
            return Name;
        }
    }
}
