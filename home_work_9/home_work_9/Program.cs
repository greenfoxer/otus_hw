﻿using System;
using System.Collections.Generic;
namespace home_work_9
{
    class Program
    {
        static void Main(string[] args)
        {
            // Задание по выводу файлов в заданной папке
            string p = @"c:\work\ChatServer\";
            string stopWord = "flag.txt";
            FileFinder finder = new FileFinder(p);
            finder.FileFound += FinderHandler;
            finder.Find(stopWord);
            ///

            // Задание по сравнению классов c помощью метода расширения
            List<TestClass> list = new List<TestClass>();
            list.Add(new TestClass("One", 1));
            list.Add(new TestClass("Eight", 8));
            list.Add(new TestClass("Three", 3));
            list.Add(new TestClass("Two", 2));
            list.Add(new TestClass("Four", 4));
            list.Add(new TestClass("Seven", 7));
            //Задание 4. Вывод сообщений при срабатывании события и результата сравнения
            var result = list.GetMax((TestClass t1, TestClass t2) => { Console.WriteLine($"Start comparing {t1.FieldStr} and {t2.FieldStr}"); return t1.FieldInt > t2.FieldInt; });
            Console.WriteLine($"Maximum is {result.FieldStr}");
        }

        static void FinderHandler(object sender, FileArgs fileArgs)
        {
            System.IO.FileInfo fi = new System.IO.FileInfo(fileArgs.Name);
            Console.WriteLine($"Full {fileArgs} short: {fi.Name}");
        }
    }
}
