﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace MessageLib
{
    [Serializable]
    public class Message: ISerializable
    {
        public string Sender { private set; get; }
        public string Body { get; private set; }
        public MessageType Type { get; private set; }
        
        // Не используется
        public static Message DeSerialize(string raw)
        {
            return JsonConvert.DeserializeObject<Message>(raw);
        }
        //Json сериализация. Для доступного вывода сообщений в консоль.
        public string Serialize()
        {
            return JsonConvert.SerializeObject(this);
        }
        // Редактирование информации о сообщении.
        public void SetMessageInfo(MessageType type=MessageType.ServerMessage, string sender = "server")
        {
            this.Type = type;
            this.Sender = sender;
        }
        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue("sender", this.Sender);
            info.AddValue("body", this.Body);
            info.AddValue("type", this.Type);
        }
        //Конструкторы
        public Message() { }
        public Message(string body,string sender="")
        {
            this.Body = body;
            this.Sender = sender;
            this.Type = MessageType.UserMessage;
        }
        public Message(SerializationInfo info, StreamingContext context)
        {
            Body = (string)info.GetValue("body", typeof(string));
            Sender = (string)info.GetValue("sender", typeof(string));
            Type = (MessageType)info.GetValue("type", typeof(MessageType));
        }
    }
}
