﻿using System;

namespace home_work_13
{
    class Program
    {
        static void Main(string[] args)
        {
            SerializerContext s = new SerializerContext();

            Request r1 = new Request("Point","XML", new Point(5,6));
            string check1 = s.Serialize(r1);
            Console.WriteLine(check1);
            Console.WriteLine();

            Request r2 = new Request("Circle", "XML", new Circle(new Point(3, 4), 5));
            string  check2 = s.Serialize(r2);
            Console.WriteLine(check2);
            Console.WriteLine();

            Request r3 = new Request("Square", "XML", new Square(new Point(6, 7), 8));
            string check3 = s.Serialize(r3);
            Console.WriteLine(check3);
            Console.WriteLine();

            Request r4 = new Request("Point", "JSON", new Point(5, 6));
            string check4 = s.Serialize(r4);
            Console.WriteLine(check4);
            Console.WriteLine();

            Request r5 = new Request("Circle", "JSON", new Circle(new Point(3, 4), 5));
            string check5 = s.Serialize(r5);
            Console.WriteLine(check5);
            Console.WriteLine();

            Request r6 = new Request("Square", "JSON", new Square(new Point(3, 4), 5));
            string check6 = s.Serialize(r6);
            Console.WriteLine(check6);
            Console.WriteLine();
        }
    }
}
