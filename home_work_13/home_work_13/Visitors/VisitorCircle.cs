﻿using System;
using System.Collections.Generic;
using System.Text;

namespace home_work_13
{
    class VisitorCircle : IVisitor
    {
        public string Visit(SerializerJSON serializer)// расширение сериализации JSON для класса Circle
        {
            Circle serializeableCircle = (Circle)serializer.Data;
            string result = $"{{\"radius\" : {serializeableCircle.Radius} , ";
            var pointrequest = new Request("Point", "JSON", serializeableCircle.Center);
            var serializerContext = new SerializerContext();
            result += $"\"centre\" : {serializerContext.Serialize(pointrequest)}}}";
            return result;
        }

        public string Visit(SerializerXML serializer) // расширение сериализации XML для класса Circle
        {
            Circle serializeableCircle = (Circle)serializer.Data;
            string result = "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n";
            result += $"<circle radius = {serializeableCircle.Radius}>";
            var pointrequest = new Request("Point", "XML", serializeableCircle.Center);
            var serializerContext = new SerializerContext();
            result += serializerContext.Serialize(pointrequest).Split('\n')[1];// отбросим заголовок XML-файла
            result += "</circle>";
            return result;
        }
    }
}
