﻿using System;
using System.Collections.Generic;
using System.Text;

namespace home_work_13
{
    class VisitorSquare : IVisitor
    {
        public string Visitor { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }

        public string Visit(SerializerJSON serializer) // расширение сериализации JSON 
        {
            Square serializeableSquare = (Square)serializer.Data;
            string result = $"{{\"edge\" : {serializeableSquare.EdgeLength} , ";
            var pointrequest = new Request("Point", "JSON", serializeableSquare.LeftTopPoint);
            var serializerContext = new SerializerContext();
            result += $"\"leftTopPoint\" : {serializerContext.Serialize(pointrequest)}}}";
            return result;
        }

        public string Visit(SerializerXML serializer) // расширение сериализации XML 
        {
            Square serializeableSquare = (Square)serializer.Data;
            string result = "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n";
            result += $"<square edge = {serializeableSquare.EdgeLength}>";
            var pointrequest = new Request("Point", "XML", serializeableSquare.LeftTopPoint);
            var serializerContext = new SerializerContext();
            result += serializerContext.Serialize(pointrequest).Split('\n')[1];// отбросим заголовок XML-файла
            result += "</square>";
            return result;
        }
    }
}
