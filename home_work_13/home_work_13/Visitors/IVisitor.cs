﻿using System;
using System.Collections.Generic;
using System.Text;

namespace home_work_13
{
    interface IVisitor // методы наблюдателей, отвечающие за расширение базового функционала классов IFigure
    {
        string Visit(SerializerJSON serializer);

        string Visit(SerializerXML serializer);
    }
}
