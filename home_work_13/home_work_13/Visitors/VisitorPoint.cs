﻿using System;
using System.Collections.Generic;
using System.Text;

namespace home_work_13
{
    class VisitorPoint : IVisitor
    {
        public string Visit(SerializerJSON serializer) // расширение сериализации JSON 
        {
            Point serializeablePoint = (Point)serializer.Data;
            string result = $"{{\"coordinateX\" : {serializeablePoint.X},\"coordinateY\" : {serializeablePoint.Y}}}";

            return result;
        }

        public string Visit(SerializerXML serializer) // расширение сериализации XML 
        {
            Point serializeablePoint = (Point)serializer.Data;
            string result = "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n";
            result += $"<point coordinateX = {serializeablePoint.X} coordinateY = {serializeablePoint.Y} </point>";
            return result;
        }
    }
}
