﻿using System;
using System.Collections.Generic;
using System.Text;

namespace home_work_13
{
    public class Circle: IFigure // все фигуры содержат только свой код, согласно принципу единства ответственности
    {
        public Point Center { get; set; }
        public int Radius { get; set; }

        public Circle(Point centre, int radius)
        {
            this.Center = centre;
            this.Radius = radius;
        }
    }
}
