﻿using System;
using System.Collections.Generic;
using System.Text;

namespace home_work_13
{
    public class Square : IFigure  // все фигуры содержат только свой код, согласно принципу единства ответственности
    {
        public Point LeftTopPoint { get; set; }
        public int EdgeLength { get; set; }
        public Square(Point leftTopPoint, int edgeLengt)
        {
            this.LeftTopPoint = leftTopPoint;
            this.EdgeLength = edgeLengt;
        }
    }
}
