﻿using System;
using System.Collections.Generic;
using System.Text;

namespace home_work_13
{
    public class Point : IFigure // все фигуры содержат только свой код, согласно принципу единства ответственности
    {
        public int X { get; set; }
        public int Y { get; set; }
        public Point(int x, int y)
        {
            this.X = x;
            this.Y = y;
        }
    }
}
