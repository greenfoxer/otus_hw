﻿using System;
using System.Collections.Generic;
using System.Text;

namespace home_work_13
{
    interface IStrategy
    {
        string Execute(IVisitor visitor, Request request); // интерфейс для стратегий
    }
}
