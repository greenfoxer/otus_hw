﻿using System;
using System.Collections.Generic;
using System.Text;

namespace home_work_13
{
    class SerializerXML : IStrategy
    {
        public IFigure Data { get; set; }

        public string Execute(IVisitor visitor, Request request)
        {
            Data = request.Data;                    // выделяем объект сериализации
            string result = visitor.Visit(this);    // выполняем метод посетителя для данной стратегии
            return result;
        }
    }
}
