﻿using System;
using System.Collections.Generic;
using System.Text;

namespace home_work_13
{
    public class SerializerContext
    {
        IStrategy strategy;
        IVisitor visitor;
        public string Serialize(Request data)
        {
            SetStrategy(data.Serializer);
            SetVisitor(data.Visitor);
            if (strategy is null)
                return "ERROR: Strategy doesn't exist!";
            if (visitor == null)
                return "ERROR: Visitor doesn't exist!";
            return strategy.Execute(visitor, data); // выполняем стратегию
        }
        void SetStrategy(string serializer) // Определяем стратегию с помощью Активатора
        {
            try
            {
                strategy = (IStrategy)Activator.CreateInstance(null, "home_work_13.Serializer" + serializer).Unwrap();
            }
            catch (TypeLoadException e)
            {
                Console.WriteLine($"ERROR: Strategy '{serializer}' not found!");
            }
            catch(Exception e)
            {
                Console.WriteLine($"ERROR: {e.Message}");
            }
        }
        void SetVisitor(string _visitor) // Определяем Посетителя с помощью Активатора
        {
            try
            {
                visitor = (IVisitor)Activator.CreateInstance(null, "home_work_13.Visitor" + _visitor).Unwrap();
            }
            catch (TypeLoadException e)
            {
                Console.WriteLine($"ERROR: Visitor '{_visitor}' not found!");
            }
            catch (Exception e)
            {
                Console.WriteLine($"ERROR: {e.Message}");
            }
        }
    }
}
