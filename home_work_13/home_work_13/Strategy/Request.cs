﻿using System;
using System.Collections.Generic;
using System.Text;

namespace home_work_13
{
    public class Request
    {
        public string Visitor;
        public readonly string Serializer;
        public IFigure Data;
        Request(string serializer, IFigure data)
        {
            this.Serializer = serializer;
            this.Data = data;
        }
        public Request(string visitor,string serializer, IFigure data)
            : this(serializer,data)
        {
            this.Visitor = visitor;
        }
    }
}
