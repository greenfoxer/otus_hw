﻿using System;
using NUnit.Framework;
using home_work_13;

namespace StrategyVisitorTests
{
    [TestFixture]
    public class Class1
    {
        [Test]
        public void CheckSimpleJSONSerialization()
        {
            SerializerContext s = new SerializerContext();

            Request r4 = new Request("Point", "JSON", new Point(5, 6));
            string check4 = s.Serialize(r4);
            string expected = "{\"coordinateX\" : 5,\"coordinateY\" : 6}";
            Assert.AreEqual(expected, check4, "Результат сериализации простого объекта отличен от ожидаемого");
        }
        [Test]
        public void CheckComplexJSONSerialization()
        {
            SerializerContext s = new SerializerContext();

            Request r6 = new Request("Square", "JSON", new Square(new Point(3, 4), 5));
            string check6 = s.Serialize(r6);
            string expected = "{\"edge\" : 5 , \"leftTopPoint\" : {\"coordinateX\" : 3,\"coordinateY\" : 4}}";
            Assert.AreEqual(expected, check6, "Результат сериализации комплексного объекта отличен от ожидаемого");
        }
        [Test]
        public void CheckWrongSerializerDescription() 
        {
            SerializerContext s = new SerializerContext();

            Request r6 = new Request("Square", "wrong", new Square(new Point(3, 4), 5));
            string check6 = s.Serialize(r6);
            bool check = check6.Contains("ERROR");
            Assert.AreEqual(true, check, "При указании неверной стратегии сериализации нет сообщения об ошибке");
        }
        [Test]
        public void CheckWrongVisitorDescription()
        {
            SerializerContext s = new SerializerContext();

            Request r6 = new Request("wrong", "XML", new Square(new Point(3, 4), 5));
            string check6 = s.Serialize(r6);
            bool check = check6.Contains("ERROR");
            Assert.AreEqual(true, check, "При указании неверного посетитля нет сообщения об ошибке");
        }
    }
}
