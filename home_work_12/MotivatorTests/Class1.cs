﻿using System;
using NUnit.Framework;
using home_work_12;
using System.IO;

namespace MotivatorTests
{
    [TestFixture]
    public class Class1
    {
        [Test]
        public void TestGeneral() // Проверим, что возвращается корректный путь
        {
            string path = @"c:\work\hws\motivator.jpg";
            Facade facade = new Facade();
            string result = facade.CreateMotivator(path, "SOME STRING TO MOTIVATE");
            bool weGot = result.Contains(@"c:\work\hws\");
            Assert.AreEqual(true, weGot, "Вернувшееся значение не содержит ожидаемой директории");
        }
        [Test]
        public void TestNewFileExist() // проверим, что файл действительно появился на диске
        {
            string path = @"c:\work\hws\motivator.jpg";
            Facade facade = new Facade();
            string result = facade.CreateMotivator(path, "SOME STRING TO MOTIVATE");
            bool weGot = File.Exists(result);
            Assert.AreEqual(true, weGot, "Файл не найден!");
        }
        [Test]
        public void TestFileChanges() // проверим, что размеры итогового изображения увеличились
        {
            string path = @"c:\work\hws\motivator.jpg";
            Facade facade = new Facade();
            string result = facade.CreateMotivator(path, "SOME STRING TO MOTIVATE");
            System.Drawing.Image img1 = System.Drawing.Image.FromFile(path);
            System.Drawing.Image img2 = System.Drawing.Image.FromFile(result);
            bool weGot = (img2.Width > img1.Width) && (img2.Height > img1.Height);
            Assert.AreEqual(true, weGot, "Файл не стал больше!");
        }
        [Test]
        public void TestNotExisitngFile() // проверим, что при неправильном  ввводе получаем ошибку
        {
            string path = @"c:\work\hws\not_exist.jpg";
            Facade facade = new Facade();
            string result = facade.CreateMotivator(path, "SOME STRING TO MOTIVATE");
            bool weGot = result.Contains(@"Error");
            Assert.AreEqual(true, weGot, "Вернувшееся значение не содержит ошибки");
        }
    }
}
