﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;

namespace home_work_12
{
    public interface IFileWorker // интерфейс некоего поставщика картинок с методами Чтения и Записи
    {
        Bitmap Read(string source); 
        string Write(Bitmap image);
    }
}
