﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.Drawing.Drawing2D;
using System.Drawing.Text;
using System.Text;
using System.IO;

namespace home_work_12
{
    public class PictureReader: IFileWorker // Реализация поставщика картинок - на локальном диске
    {
        FileInfo inputFile; //Сохраним информацию о первоначальном файле

        string GenerateName() // cгенерируем никальное имя на основе даты
        {
            return DateTime.Now.ToBinary().ToString();
        }

        public string Write(Bitmap image) // запишем результат в файл
        {
            string path = $"{inputFile.DirectoryName}\\{GenerateName()}.jpg"; // возьмем директорию первоначального файла + случайное название + расширение
            image.Save(path);
            return path;
        }

        public Bitmap Read(string source) // считываем файл и возвращаем Bitmap; сохраняеминформацию о файле
        {
            inputFile = new FileInfo(source);
            return new Bitmap(source);
        }
    }
}
