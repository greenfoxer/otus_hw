﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;
namespace home_work_12
{
   public  class Motivator: IDisposable
    {
        Bitmap picture; // первоачальная картинка
        Bitmap background; // заготовка с фоном
        public Bitmap result; // результат
        public MotivatorSettings Settings; // класс с настройками для работы
 
        public Motivator(Bitmap bitmap)
        {
            picture = bitmap;
            Settings = new MotivatorSettings();
        }

        public void Dispose() // очистим все промежуточные буферы
        {
            picture.Dispose();
            background.Dispose();
            result.Dispose();
        }
        public void GenerateBackground() // Создадим фон для картинки - учитывает все рамки
        {
            int borderThin = Settings.BorderThin;
            int BorderFat = Settings.BorderFat;
            background = new Bitmap(picture.Width + borderThin * 2, picture.Height + BorderFat + borderThin);
            using (var g = Graphics.FromImage(background))
            {
                using (Brush border = new SolidBrush(Settings.FrameColor))
                {
                    g.FillRectangle(border, 0, 0, background.Width, background.Height);
                }
                result = background;
            }
        }

        public void ComposeBackgrounAndImage() // наложим картинку на фон
        {
            int borderThin = Settings.BorderThin;
            using (var g = Graphics.FromImage(background))
            {
                g.DrawImage(picture, borderThin, borderThin);
            }
            result = background;
        }

        public void AddBorder() // добавим к картинке тонкую рамочку
        {
            using (var g = Graphics.FromImage(picture))
                g.DrawRectangle(new Pen(Settings.BorderColor, 5), new Rectangle(0, 0, picture.Width, picture.Height));
            result = picture;
        }
        public void AddText(string text) // добавим текст
        {
            using (var sf = new StringFormat()
            {
                Alignment = StringAlignment.Center,
                LineAlignment = StringAlignment.Center,
            })
            {
                using (var gr = Graphics.FromImage(result)) // разместим текст в центре прямоугольника, который поместим в свободное пространство под оригинальной картинкой
                {
                    ScaleTextFont(gr,text,result.Width,sf, result.Height - picture.Height);
                    gr.DrawString(text, Settings.TextFont, Settings.TextColor, new Rectangle(0, picture.Height+Settings.BorderThin/2, result.Width, result.Height - picture.Height), sf); ;
                }
            }
        }

        private void ScaleTextFont(Graphics gr, string text, int width, StringFormat sf, int expected) // отмаштабируем размер текста, если требуется
        {
            SizeF strF = gr.MeasureString(text, Settings.TextFont, result.Width, sf); // рассчитаем предполагаемый прямоугольник, куда вписывается наш текст
            while (strF.Height > expected) // если его высота больше оставленного нами свободного пространства - уменьшим шрифт на 1 и поаторим проверку
            {
                Settings.TextFontSize--;
                strF = gr.MeasureString(text, Settings.TextFont, result.Width, sf);
            }
        }
    }
}
