﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;

namespace home_work_12
{
    public class MotivatorSettings
    {
        public int BorderThin { get; set; } = 20;
        public int BorderFat { get; set; } = 150;
        public Color FrameColor { get; set; } = Color.LightSkyBlue;
        public Brush BorderColor { get; set; } = Brushes.LightGray;
        public int TextFontSize { get; set; } = 36;
        public Font TextFont { get { return new Font("Arial", TextFontSize); } set { } }
        public Brush TextBrush { get; set; } = SystemBrushes.WindowText;
        public Brush TextColor { get; set; } = Brushes.LightGray;
    }
}
