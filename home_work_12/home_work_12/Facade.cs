﻿using System;
using System.Collections.Generic;
using System.Text;

namespace home_work_12
{
    public class Facade // фасад для изготовления мотиватора
    {
        public string CreateMotivator(string image, string message) // скрываем от пользоавтеля множество шагов и технических деталей
        {
            IFileWorker phisicalAccess = new PictureReader();
            string result;

            try
            {
                var pic = phisicalAccess.Read(image); // прочитаем изображение
                using (var mot = new Motivator(pic))
                {
                    mot.GenerateBackground(); // нарисуем подложку некоего цвета
                    mot.AddBorder();            // добавим к оригинальной кртинке небольшую рамку
                    mot.ComposeBackgrounAndImage(); // наложим оригинал на подложку
                    mot.AddText(message);       // добавим подпись
                    result = phisicalAccess.Write(mot.result); // сгенерируем новый файл
                }
            }
            catch (Exception e)
            {
                result = $"Error! {e.Message}";
            }
           

            return result;
        }
    }
}
