﻿using System;

namespace home_work_12
{
    class Program
    {
        static void Main(string[] args)
        {
            Facade facade = new Facade();
            Console.WriteLine(facade.CreateMotivator(@"c:\work\hws\motivatr.jpg", "THIS IS THE STORY ABOUT OLD WHITE WOLF WHO LIVES IN THE CENTRE OF GALAXY"));
            Console.WriteLine(facade.CreateMotivator(@"c:\work\hws\motivator.jpg", "THIS IS THE STORY ABOUT OLD WHITE WOLF WHO LIVES IN THE CENTRE OF GALAXY"));
            Console.WriteLine(facade.CreateMotivator(@"c:\work\hws\motivator.jpg", "GOOGD LUCK!"));
        }
    }
}
