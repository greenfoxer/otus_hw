﻿namespace UserLibrary
{
    public interface IRabbitConfig
    {
        string Host { get; set; }
        string VirtHost { get; set; }
        string UserName { get; set; }
        string Pass { get; set; }
    }
}