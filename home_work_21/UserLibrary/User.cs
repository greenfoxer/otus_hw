﻿using System.Text.Json;

namespace UserLibrary
{
    public class User
    {
        public string Name { get; set; }
        public int Age { get; set; }
        public string Email { get; set; }

        public User(string name, int age, string email)
        {
            this.Name = name;
            this.Age = age;
            this.Email = email;
        }

        public User()
        { }

        public override string ToString()
        {
            return JsonSerializer.Serialize(this);
        }
    }
}
