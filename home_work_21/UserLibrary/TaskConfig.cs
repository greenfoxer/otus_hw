﻿using System;
using System.Collections.Generic;
using System.Text;

namespace UserLibrary
{
    public class TaskConfig: ITaskConfig
    {
        public TaskConfig(string queue)
        {
            Queue = queue;
        }

        public string Queue { get; set; }
    }
}
