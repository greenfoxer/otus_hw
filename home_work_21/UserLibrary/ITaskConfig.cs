﻿namespace UserLibrary
{
    public interface ITaskConfig
    {
        string Queue { get; set; }
    }
}