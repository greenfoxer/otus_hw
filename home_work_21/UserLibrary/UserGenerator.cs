﻿using System;
using System.Collections.Generic;
using System.Text;
using Bogus;

namespace UserLibrary
{
    public static class RandomUserGenerator
    {
        public static User GenerateOne()
        {
            var customersFaker = CreateFaker();
            return customersFaker.Generate();
        }

        private static Faker<User> CreateFaker()
        {
            var customersFaker = new Faker<User>()
                .CustomInstantiator(f => new User())
                .RuleFor(u => u.Name, (f, u) => f.Name.FirstName())
                .RuleFor(u => u.Email, (f, u) =>
                                                    {
                                                        int random = new Random().Next(1,100);
                                                        if(random <= 80)
                                                            return f.Internet.Email(u.Name);
                                                        return "";
                                                    })
                .RuleFor(u => u.Age, (f, u) => f.Random.Int(0,120));

            return customersFaker;
        }
    }
}
