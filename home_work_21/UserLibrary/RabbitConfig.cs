﻿using System;
using System.Collections.Generic;
using System.Text;

namespace UserLibrary
{
    public class RabbitConfig : IRabbitConfig
    {
        public string Host { get; set; }
        public string VirtHost { get; set; }
        public string UserName { get; set; }
        public string Pass { get; set; }

        public RabbitConfig(string host, string virtHost, string userName, string pass)
        {
            this.Host = host;
            this.VirtHost = virtHost;
            this.UserName = userName;
            this.Pass = pass;
        }

    }
}
