﻿using System;
using System.Collections.Generic;
using System.Text;
using RabbitMQ;
using RabbitMQ.Client;
using UserLibrary;

namespace home_work_21
{
    public class RabbitSender : IDisposable
    {
        private readonly ConnectionFactory _factory;
        private readonly IConnection connection;
        private readonly IModel channel;

        public RabbitSender(IRabbitConfig cfg)
        {
            _factory = new ConnectionFactory() { HostName = cfg.Host, VirtualHost =cfg.VirtHost, UserName = cfg.UserName, Password = cfg.Pass };
            connection = _factory.CreateConnection();
            channel = connection.CreateModel();
        }

        public void Dispose()
        {
            channel.Dispose();
            connection.Dispose();
        }

        public bool SendMessage(string message, ITaskConfig taskCfg)
        {
            try
            {
                channel.QueueDeclare(queue: taskCfg.Queue,
                                        durable: false,
                                        exclusive: false,
                                        autoDelete: false,
                                        arguments: null);

                var body = Encoding.UTF8.GetBytes(message);

                channel.BasicPublish(exchange: "",
                                        routingKey: taskCfg.Queue,
                                        basicProperties: null,
                                        body: body);   
            }
            catch
            {
                return false;
            }
            return true;
        }
    }
}
