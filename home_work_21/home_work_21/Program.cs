﻿using Microsoft.Extensions.Configuration;
using System;
using System.IO;
using System.Threading;
using UserLibrary;

namespace home_work_21
{
    class Program
    {
        static IRabbitConfig rcfg;
        static ITaskConfig tcfg;

        static void ConfigureService()
        {
            var builder = new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory()).AddJsonFile("appconfig.json", optional: true, reloadOnChange: true);
            IConfigurationRoot configuration = builder.Build();
            rcfg = new RabbitConfig(configuration["rabbit:host"], configuration["rabbit:virthost"], configuration["rabbit:username"], configuration["rabbit:passwd"]);
            tcfg = new TaskConfig(configuration["homework:queuename"]);
        }

        static void Main(string[] args)
        {
            ConfigureService();

            User user;
            using ( var rabbitSender = new RabbitSender(rcfg))
            {
                while (true)
                {
                    user = RandomUserGenerator.GenerateOne();
                    if (rabbitSender.SendMessage(user.ToString(), tcfg))
                        Console.WriteLine($"Sended: {user.ToString()}");
                    Thread.Sleep(1000);
                }
            }
        }
    }
}
