﻿using Microsoft.Extensions.Configuration;
using System;
using System.IO;
using System.Threading;
using UserLibrary;

namespace ConsoleReceiver
{
    class Program
    {
        static RabbitConfig rcfg;
        static TaskConfig tcfg;

        static void ConfigureService()
        {
            var builder = new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory()).AddJsonFile("appconfig.json", optional: true, reloadOnChange: true);
            IConfigurationRoot configuration = builder.Build();
            rcfg = new RabbitConfig(configuration["rabbit:host"], configuration["rabbit:virthost"], configuration["rabbit:username"], configuration["rabbit:passwd"]);
            tcfg = new TaskConfig(configuration["homework:queuename"]);
        }

        static void Main(string[] args)
        {
            ConfigureService();
            using (var receiver = new RabbitReceiver(rcfg))
            {
                while (true)
                {
                    string result = receiver.ReceiveMessage(tcfg);
                    Console.WriteLine(string.IsNullOrEmpty(result) ? "Empty message!" : result);
                    Thread.Sleep(1000);
                }
            }
        }
    }
}
