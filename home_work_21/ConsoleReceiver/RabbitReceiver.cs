﻿using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using RabbitMQ.Client.MessagePatterns;
using System;
using System.Collections.Generic;
using System.Text;
using System.Text.Json;
using UserLibrary;

namespace ConsoleReceiver
{
    public class RabbitReceiver : IDisposable
    {
        private readonly ConnectionFactory _factory; 
        private readonly IConnection connection;
        private readonly IModel channel;

        public RabbitReceiver(RabbitConfig cfg)
        {
            _factory = new ConnectionFactory() { HostName = cfg.Host, VirtualHost = cfg.VirtHost, UserName = cfg.UserName, Password = cfg.Pass };
            connection = _factory.CreateConnection();
            channel = connection.CreateModel();
        }

        public void Dispose()
        {
            channel.Dispose();
            connection.Dispose();
        }

        public string ReceiveMessage(ITaskConfig taskCfg)
        {
            string result = null;

            channel.QueueDeclare(queue: taskCfg.Queue,
                                 durable: false,
                                 exclusive: false,
                                 autoDelete: false,
                                 arguments: null);

            var rawData = channel.BasicGet(taskCfg.Queue, false);

            if (rawData != null)
            {
                string message = Encoding.UTF8.GetString(rawData.Body);
                var user = JsonSerializer.Deserialize<User>(message);
                if (!string.IsNullOrEmpty(user.Email))
                {
                    channel.BasicAck(rawData.DeliveryTag, false);
                    result = $"Received: {user.ToString()}";
                }
                else
                {
                    channel.BasicReject(rawData.DeliveryTag, false);
                    result = $"Rejected: {user.ToString()}";
                }
            } 
            return result;
        }
    }
}
