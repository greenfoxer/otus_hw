﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EchoLibrary
{
    public interface IDataWriter
    {
        void PrintLine(string data);
        void Print(string data);
    }
}
