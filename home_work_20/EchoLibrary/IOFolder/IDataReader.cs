﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EchoLibrary
{
    public interface IDataReader
    {
        string Read();
    }
}
