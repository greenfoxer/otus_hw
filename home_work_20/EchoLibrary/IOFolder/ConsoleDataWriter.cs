﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EchoLibrary
{
    public class ConsoleDataWriter : IDataWriter
    {
        public void Print(string data)
        {
            Console.Write(data);
        }

        public void PrintLine(string data)
        {
            Console.WriteLine(data);
        }
    }
}
