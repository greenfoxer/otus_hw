﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EchoLibrary
{
    public abstract class AbstractIOClass : IDataReader, IDataWriter
    {
        internal IDataReader Reader;
        internal IDataWriter Writer;

        public AbstractIOClass(IDataReader reader, IDataWriter writer)
        {
            this.Reader = reader;
            this.Writer = writer;
        }

        public string Read()
        {
            return Reader.Read();
        }
        public void Print(string data)
        {
            Writer.Print(data);
        }
        public void PrintLine(string data)
        {
            Writer.PrintLine(data);
        }
        public abstract string Input(string inv);
    }
}
