﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EchoLibrary
{
    public class PythonStyleConsoleIO : AbstractIOClass
    {
        public PythonStyleConsoleIO(ConsoleDataReader reader, ConsoleDataWriter writer)
            :base(reader,writer)
        {         }

        public override string Input(string inv)
        {
            PrintLine(inv);
            return Read();
        }
    }
}
