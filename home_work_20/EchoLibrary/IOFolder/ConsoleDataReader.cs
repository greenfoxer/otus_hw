﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EchoLibrary
{
    public class ConsoleDataReader : IDataReader
    {
        public string Read()
        {
            return Console.ReadLine();
        }
    }
}
