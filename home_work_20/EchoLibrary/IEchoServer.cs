﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EchoLibrary
{
    public interface IEchoServer
    {
        public IDataReader Reader { get; set; }
        public IDataWriter Printer { get; set; }
        public IEchoService EchoService { get; set; }
        void Process();
    }
}
