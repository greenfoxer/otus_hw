﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EchoLibrary
{
    public abstract class AbstractEchoServer : IEchoServer
    {
        public IDataReader Reader { get; set; }
        public IDataWriter Printer { get; set; }
        public IEchoService EchoService { get; set; }

        public void Process()
        {
            ShowInvitation();
            DecorateUserInputByPromt();
            ReadInput();
            ExecuteService();
            ShowAnswer();
        }

        internal abstract void ReadInput();
        internal abstract void ShowAnswer();
        internal abstract void ExecuteService();
        internal abstract void DecorateUserInputByPromt();
        internal abstract void ShowInvitation();
    }
}
