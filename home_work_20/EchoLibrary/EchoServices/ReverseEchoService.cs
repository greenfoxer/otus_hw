﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace EchoLibrary
{
    public class ReverseEchoService : IEchoService
    {
        public string Echo(string data)
        {
            return string.Join("", data.ToCharArray().Reverse());
        }
    }
}
