﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EchoLibrary
{
    public class BasicEchoServer : AbstractEchoServer
    {
        private string _userData;
        private string _serviceAnswer;

        internal override void ShowAnswer()
        {
            Printer.PrintLine(_serviceAnswer);
        }

        internal override void ExecuteService()
        {
            _serviceAnswer = EchoService.Echo(_userData);
        }

        internal override void DecorateUserInputByPromt()
        {
            Printer.Print("> ");
        }

        internal override void ShowInvitation()
        {
            Printer.PrintLine("I listen to you my friend!");
        }

        internal override void ReadInput()
        {
            _userData = Reader.Read();
        }

        public BasicEchoServer(IDataReader input, IDataWriter output, IEchoService service)
        {
            this.Reader = input;
            this.Printer = output;
            this.EchoService = service;
        }
    }
}
