﻿using System;
using EchoLibrary;
namespace home_work_20
{
    class Program
    {
        static void Main(string[] args)
        {
            PythonStyleConsoleIO pyIO = new PythonStyleConsoleIO(new ConsoleDataReader(), new ConsoleDataWriter());
            IEchoServer server = new BasicEchoServer(new ConsoleDataReader(),new ConsoleDataWriter(),new ReverseEchoService());
            server.Process();
            server = new BasicEchoServer(pyIO, pyIO, new ReverseEchoService());
            server.Process();
        }
    }
}
