﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace Graph
{
    public class RoadMap<T,V>: Graph<T,V> where T : class where V : INode<T>
    {
        public RoadMap()
            :base()
        { 
        
        }
        List<T> last_route;
        T last_start;
        T last_finish;
        public List<T> FindPath(Node<T> start, Node<T> finish)
        {
            Dictionary<Node<T>, int> costs = new Dictionary<Node<T>, int>();
            Dictionary<Node<T>, Node<T>> parents = new Dictionary<Node<T>, Node<T>>();
            List<Node<T>> processed = new List<Node<T>>();
            last_start = start.Data ;
            last_finish = finish.Data;
            foreach (var nod in Nodes)
                costs.Add(nod, int.MaxValue);

            costs[start] = 0;

            Node<T> node = FindLowestNode(costs, processed);
            while (node != null)
            {
                int cost = costs[node];
                foreach (var t in node.Links)
                {
                    int new_cost = cost + t.Value;
                    if (costs[t.Key] > new_cost)
                    {
                        costs[t.Key] = new_cost;
                        if (parents.Keys.Contains(t.Key))
                            parents[t.Key] = node;
                        else
                            parents.Add(t.Key, node);
                    }
                }
                processed.Add(node);
                node = FindLowestNode(costs, processed);
            }
            List<T> route = new List<T>();
            node = finish;
            if (!(costs[node] == int.MaxValue))
            {
                do
                {
                    route.Add(node.Data);
                    node = parents[node];
                } while (node != start);
                route.Add(start.Data);
                route.Reverse();
            }
            else
            {
                route.Add(start.Data);
            }
            last_route = route;
            return route;
        }
        Node<T> FindLowestNode(Dictionary<Node<T>, int> costs, List<Node<T>> processed)
        {
            Node<T> node = null;
            int low_cost = int.MaxValue;
            foreach (var pair in costs)
            {
                int cost = pair.Value;
                if (cost < low_cost && !processed.Contains(pair.Key))
                {
                    low_cost = cost;
                    node = pair.Key;
                }
            }
            return node;
        }
        public string PrintLastFinfingRoute()
        {
            if (last_route == null)
                return "There is no finding route!";
            if (last_route.Count == 0)
                return "There is no finding route!";
            if (last_route.Count == 1)
                return $"There is no route from {last_start} to {last_finish}!";
            string result = "Route: \n";
            int i=0;
            int total = 0;
            do
            {
                result += $"\tFrom {last_route[i]} ";
                int weigth = GetNodeByData(last_route[i]).Links[GetNodeByData(last_route[++i])];
                total += weigth;
                result += $"To {last_route[i]} for {weigth} \n";
            } while (i < last_route.Count - 1);
            result += $"You are in the finish point in {total} steps!";
            return result;
        }
    }
}
