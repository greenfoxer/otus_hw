﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;
namespace Graph
{
    public static class Serializer
    {
        public static string SerializeLinks<T>(this Graph<T, INode<T>> graph) where T : class
        {
            return JsonConvert.SerializeObject(graph.GenerateMatrix());
        }
        public static int[][] DeSerializeLinks<T>(this Graph<T, INode<T>> graph, string data) where T : class
        {
            return JsonConvert.DeserializeObject<int[][]>(data);
        }
        public static List<string> SerializeNodes<T>(this Graph<T, INode<T>> graph) where T : class
        {
            List<string> result = new List<string>();
            foreach (var item in graph.Nodes)
                result.Add(JsonConvert.SerializeObject(item));
            return result;
        }
        public static List<Node<T>> DeSerializeNodes<T>(this Graph<T, INode<T>> graph, List<string> data) where T : class
        {
            List<Node<T>> result = new List<Node<T>>();
            foreach (string item in data)
                result.Add(JsonConvert.DeserializeObject<Node<T>>(item));
            return result;
        }
    }
}
