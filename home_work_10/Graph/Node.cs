﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.Serialization;
using System.Linq;

namespace Graph
{
    public class Node<T>: INode<T>, ISerializable
    { 
        public int Id { get; set; }
        public T Data { get; set; } = default(T);
        [NonSerialized]
        public Dictionary<Node<T>, int> Links = new Dictionary<Node<T>, int>();
        public Node(T data, int id)
        {
            //Links = new Dictionary<Node<T>, int>();
            this.Data = data;
            this.Id = id;
        }
        public void AddLink(Node<T> node, int weigth)
        {
            Links.Add(node, weigth);
        }
        public void RemoveLink(Node<T> node)
        {
            Links.Remove(node);
        }

        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue("data", this.Data);
            info.AddValue("id", this.Id);
        }
        public Node()
        { }

        public Node(SerializationInfo info, StreamingContext context)
        {
            Data = (T)info.GetValue("data", typeof(T));
            Id = (int)info.GetValue("sender", typeof(int));
        }
    }
}
