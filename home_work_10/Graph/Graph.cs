﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace Graph
{
    public class Graph<T,V> where T : class where V : INode<T>
    {
        protected internal List<Node<T>> Nodes;
        public int Count { get { return Nodes.Count; } }
        public Node<T> AddNode(T data)
        {
            Node<T> newNode = new Node<T>(data, Nodes.Count + 1);
            Nodes.Add(newNode);
            return newNode;
        }
        public void AddNode(Node<T> node)
        {
            Nodes.Add(node);
        }
        public Node<T> AddNode(T data, int id)
        {
            Node<T> newNode = new Node<T>(data, id);
            Nodes.Add(newNode);
            return newNode;
        }
        public bool Contains(T data)
        {
            foreach (Node<T> node in Nodes)
                if (node.Data == data)
                    return true;
            return false;
        }

        public void Remove(T data)
        {
            Node<T> node = Nodes.First(p => p.Data == data);
            node = Nodes.First(t => t.Data == data);
            RemoveLinks(node);
        }
        public void Remove(Node<T> node)
        {
            if (Nodes.Contains(node))
            {
                Nodes.Remove(node);
                RemoveLinks(node);
            }
        }
        void RemoveLinks(Node<T> node)
        {
            if (node != null)
            {
                Nodes.Remove(node);
                foreach (Node<T> n in Nodes)
                    n.RemoveLink(node);
            }
        }
        public Graph()
        {
            Nodes = new List<Node<T>>();
        }
        public Node<T> GetNodeById(int id)
        {
            return Nodes.FirstOrDefault(t => t.Id == id);
        }
        public Node<T> GetNodeByData(T data)
        {
            return Nodes.FirstOrDefault(t => t.Data == data);
        }
        public void ParseMatrix(int[][] matrix)
        {
            if (matrix.Length != Nodes.Count)
                throw new Exception("Number of matrix elements not equals to number of nodes");
            int len = Nodes.Count;
            for (int i = 0; i < len; i++)
                for (int j = 0; j < len; j++)
                    if (matrix[i][j] != 0)
                    {
                        GetNodeById(i + 1).AddLink(GetNodeById(j + 1), matrix[i][j]);
                    }
        }
        public int[][] GenerateMatrix()
        {
            RecalcIDs();
            int[][] matrix = new int[Nodes.Count][];
            for (int i = 0; i < Nodes.Count; i++)
                matrix[i] = new int[Nodes.Count];
            foreach (var item in Nodes)
            {
                foreach (var link in item.Links.Keys)
                    matrix[item.Id - 1][ link.Id-1] = item.Links[link];
            }
            return matrix;
        }
        public void RecalcIDs()
        {
            int startId = 1;
            foreach (var node in Nodes)
                node.Id = startId++;
        }
    }
}