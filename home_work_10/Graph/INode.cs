﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Graph
{
    public interface INode<T>
    {
        public int Id { get; set; }
        public T Data { get; set; }
    }
}
