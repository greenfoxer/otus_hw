﻿using System;
using Graph;
using System.Collections.Generic;
using CommonClasses;

namespace home_work_10
{
    class Program
    {
        static void Main(string[] args)
        {
            Node<string> node = new Node<string>("zhopa", 1);
            RoadMap<string, INode<String>> g = new RoadMap<string, INode<string>>();
            Node<string> one = new Node<string>("node1", 1);
            Node<string> two = new Node<string>("node2", 2);
            Node<string> three = new Node<string>("node3", 3);
            Node<string> four = new Node<string>("node4", 4);
            //one.AddLink(two, 6);
            //one.AddLink(three, 2);
            //two.AddLink(four, 1);
            //three.AddLink(two, 3);
            //three.AddLink(four, 5);
            int[][] matrix = { new[]{ 0, 6, 2, 0 }, new[]{ 0, 0, 0, 1 }, new[]{ 0, 3, 0, 5 }, new[]{ 0, 0, 0, 0 } };
            string text = "[[0,6,2,0],[0,0,0,1],[0,3,0,5],[0,0,0,0]]";
            g.AddNode(one);
            g.AddNode(two);
            g.AddNode(three);
            g.AddNode(four);
            g.ParseMatrix(matrix);
            bool o = g.Contains("node4");
            int[][] newMatrix = g.GenerateMatrix();
            string template = g.SerializeLinks();
            int[][] matrix3 = g.DeSerializeLinks(template);
            List<string> nodes = g.SerializeNodes();
            string pew = string.Join("", nodes);
            RoadMap<string, INode<String>> g2 = new RoadMap<string, INode<string>>();
            g2.DeSerializeNodes(nodes);
            List<string> route = g.FindPath(one, four);
            foreach (string s in route)
                Console.Write($"{s} -> ");


            g = new RoadMap<string, INode<string>>();
            string jsonNodes = "{\"Id\":1,\"Data\":\"node1\"}\n{\"Id\":2,\"Data\":\"node2\"}\n{\"Id\":3,\"Data\":\"node3\"}\n{\"Id\":4,\"Data\":\"node4\"}";
            List<string> arr = new List<string>(jsonNodes.Split('\n'));
            foreach (var item in arr)
            {
                Console.WriteLine(item);
            }
            var nodes1 = g.DeSerializeNodes(arr);
            foreach (var n in nodes1)
                g.AddNode(n);


            RoadMap<Town, INode<Town>> g3 = new RoadMap<Town, INode<Town>>();
            Town t1 = new Town("AAAAA", 1235);
            Town t2 = new Town("BBBBB", 2345);
            Town t3 = new Town("CCCCC", 3456);
            Town t4 = new Town("DDDDD", 4567);
            g3.AddNode(t1);g3.AddNode(t2);g3.AddNode(t3);g3.AddNode(t4);
            int[][]  matrix1 = { new[] { 0, 6, 2, 0 }, new[] { 0, 0, 0, 1}, new[] { 0, 3, 0, 5 }, new[] { 0, 0, 0, 0 } };
            g3.ParseMatrix(matrix1);
            g3.PrintLastFinfingRoute();
            List<Town> route3 = g3.FindPath(g3.GetNodeByData(t1), g3.GetNodeByData(t4));
            List<string> nodes3 = g3.SerializeNodes();
            string te = g3.PrintLastFinfingRoute();
            Console.WriteLine( te);



        }
    }
}
