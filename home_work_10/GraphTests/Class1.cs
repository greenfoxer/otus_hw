﻿using System;
using NUnit.Framework;
using Graph;
using System.Collections.Generic;
using CommonClasses;

namespace GraphTests
{
    [TestFixture]
    public class Class1
    {
        RoadMap<Town, INode<Town>> FillGraphByTowns(int step = 1)
        {
            RoadMap<Town, INode<Town>> g = new RoadMap<Town, INode<Town>>();
            Town t1 = new Town("AAAAA", 1235);
            Town t2 = new Town("BBBBB", 2345);
            Town t3 = new Town("CCCCC", 3456);
            Town t4 = new Town("DDDDD", 4567);
            g.AddNode(t1); g.AddNode(t2); g.AddNode(t3); g.AddNode(t4);
            string links;
            int[][] matrox = null;
            if (step == 1)
            {
                links = "[[0,6,2,0],[0,0,0,1],[0,3,0,5],[0,0,0,0]]";
                matrox = g.DeSerializeLinks(links);
            }
            else if (step == 2)
            {
                int[][] matrox1 = { new[] { 0, 6, 2, 0 }, new[] { 0, 0, 0, 0 }, new[] { 0, 3, 0, 0 }, new[] { 0, 0, 0, 0 } };
                matrox = matrox1;
            }
            g.ParseMatrix(matrox);
            return g;
        }
        Graph<string, INode<string>> FillGraph(int step = 1)
        {
            Graph<string, INode<String>> g=new Graph<string, INode<string>>(); ;
           
            Node<string> one = new Node<string>("node1", 1);
            Node<string> two = new Node<string>("node2", 2);
            Node<string> three = new Node<string>("node3", 3);
            Node<string> four = new Node<string>("node4", 4);
            if (step <= 3)
            {
                g.AddNode(one);
                g.AddNode(two);
                g.AddNode(three);
                g.AddNode(four);
            }
            else
            {
                string jsonNodes = "{\"Id\":1,\"Data\":\"node1\"}\n{\"Id\":2,\"Data\":\"node2\"}\n{\"Id\":3,\"Data\":\"node3\"}\n{\"Id\":4,\"Data\":\"node4\"}";
                List<string> arr = new List<string>(jsonNodes.Split('\n'));
                foreach (var item in arr)
                {
                    Console.WriteLine(item);
                }
                var nodes = g.DeSerializeNodes(arr);
                foreach (var n in nodes)
                    g.AddNode(n);
            }
            if (step == 1)
            {
                one.AddLink(two, 6);
                one.AddLink(three, 2);
                two.AddLink(four, 1);
                three.AddLink(two, 3);
                three.AddLink(four, 5);
            }
            if (step == 2)
            {
                int[][] matrix = { new[] { 0, 6, 2, 0 }, new[] { 0, 0, 0, 1 }, new[] { 0, 3, 0, 5 }, new[] { 0, 0, 0, 0 } };
                g.ParseMatrix(matrix);
            }
            if (step >=3 )
            {
                string links = "[[0,6,2,0],[0,0,0,1],[0,3,0,5],[0,0,0,0]]";
                var matrox = g.DeSerializeLinks(links);
                g.ParseMatrix(matrox);
            }
            return g;
        }
        [Test]
        public void GraphCreation()
        {
            var graph = FillGraph();
            Assert.AreEqual(4, graph.Count, "Number of nodes in graph is wrong.");
        }
        [Test]
        public void AlternativeGraphLinksDefinitions()
        {
            var graph = FillGraph(2);
            int node3Links = graph.GetNodeById(3).Links.Count;
            Assert.AreEqual(2, node3Links, "Number of links are not equal!");

            graph = FillGraph(3);
            node3Links = graph.GetNodeById(3).Links.Count;
            Assert.AreEqual(2, node3Links, "Number of links are not equal!");
        }
        [Test]
        public void AlternativeGraphNodesDefinitions()
        {
            var graph = FillGraph(4);
            int node3Links = graph.GetNodeById(3).Links.Count;
            Assert.AreEqual(2, node3Links, "Number of links are not equal!");
        }
        [Test]
        public void RoadMapTownCheck()
        {
            var roadMap = FillGraphByTowns();
            Assert.AreEqual(4, roadMap.Count, "Number of Towns in graph is wrong.");
        }
        [Test]
        public void RoadMapRouteCheck()
        {
            var roadMap = FillGraphByTowns();
            Assert.AreEqual(4, roadMap.Count, "Number of Towns in graph is wrong.");
            var route = roadMap.FindPath(roadMap.GetNodeById(1), roadMap.GetNodeById(4));
            Assert.AreEqual(4, route.Count, "Number of route points is wrong!");
            string expectedRoute = "Route: \n\tFrom AAAAA To CCCCC for 2 \n\tFrom CCCCC To BBBBB for 3 \n\tFrom BBBBB To DDDDD for 1 \nYou are in the finish point in 6 steps!";
            Assert.AreEqual(expectedRoute, roadMap.PrintLastFinfingRoute(), "Routes are not the same!");
        }
        [Test]
        public void RoadMapNoRouteCheck()
        {
            var roadMap = FillGraphByTowns(2);
            Assert.AreEqual(4, roadMap.Count, "Number of Towns in graph is wrong.");
            var route = roadMap.FindPath(roadMap.GetNodeById(1), roadMap.GetNodeById(4));
            string expectedRoute = $"There is no route from {roadMap.GetNodeById(1).Data} to {roadMap.GetNodeById(4).Data}!";
            Assert.AreEqual(expectedRoute, roadMap.PrintLastFinfingRoute(), "Routes are not the same!");
        }
    }
}
