﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CommonClasses
{
    public class Town
    {
        public string Name { get; set; }
        public int Population { get; set; }
        public Town(string name, int population)
        {
            this.Name = name;
            this.Population = population;
        }
        public override string ToString()
        {
            return Name;
        }
    }
}
