﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace home_work_15
{
    class DataGenerator
    {
        public int[] Array { get; private set; }
        int BorderMin { get; set; }
        int BorderMax { get; set; }
        long Result = 0;
        long Count { get; set; }

        public DataGenerator(long num, int min = 0, int max = 100)
        {
            BorderMax = max;
            BorderMin = min;
            Array = new int[num];
            Count = num;
            Stopwatch stopwatch = new Stopwatch();
            stopwatch.Start();
            //ParallelFillArray();
            FillArray();
            stopwatch.Stop();
            Console.WriteLine($"Array filled in {stopwatch.Elapsed}");
        }

        public void FillArray()
        {
            var randomizer = new Random();
            for (long i = 0; i < Count; i++)
            {
                Array[i] = randomizer.Next(BorderMin, BorderMax);
               // Result += Array[i];
            }
        }
        public void ParallelFillArray()
        {
            var randomizer = new Random();
            Parallel.For(0, Count, (index) => { Array[index] = randomizer.Next(BorderMin, BorderMax); /*Interlocked.Add(ref Result, Array[index]);*/ }) ;
        }

        public long GetResult() => Result;

        public void PrintArray()
        {
            for (long i = 0; i < Count; i++)
            {
                Console.Write(Array[i] + ", ");
            }
            Console.WriteLine();
        }

    }
}
