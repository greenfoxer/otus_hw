﻿using System;

namespace home_work_15
{
    class Program
    {
        static void Main(string[] args)
        {
            var m = new DataGenerator(100_000_000);// (1000000000);
            Console.WriteLine($"Calculated sum is {m.GetResult()}");

            SumStrategyContext context = new SumStrategyContext(m.Array, "single");
            long result = context.ExecuteStrategy();

            context = new SumStrategyContext(m.Array, "parallel_interlocked");
            result = context.ExecuteStrategy();

            context = new SumStrategyContext(m.Array, "parallel_lock");
            result = context.ExecuteStrategy();

            context = new SumStrategyContext(m.Array, "plinq");
            result = context.ExecuteStrategy();

            context = new SumStrategyContext(m.Array, "linq");
            result = context.ExecuteStrategy();
        }
    }
}
