﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace home_work_15
{
    class ParallelLockSummator : ISummator
    {
        long result = 0;
        object locker = new object();
        public long Sum(int[] array)
        {
            var response = Parallel.For(0, array.Length, (i) => {
                lock (locker)
                {
                    result += array[i];
                }
            });
            return result;
        }
    }
}
