﻿using System;
using System.Collections.Generic;
using System.Text;

namespace home_work_15
{
    class OneThreadSummator : ISummator
    {
        public long Sum(int[] array)
        {
            long result = 0;
            for (long i = 0; i < array.Length; i++)
                result += array[i];
            return result;
        }
    }
}
