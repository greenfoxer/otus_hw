﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Timers;
using System.Diagnostics;
namespace home_work_15
{
    class SumStrategyContext
    {
        int[] array;
        ISummator summator;
        string literal;

        public SumStrategyContext(int[] arr, string lit)
        {
            literal = lit;
            array = arr;
            SetSumator(lit);
        }

        public long ExecuteStrategy()
        {
            long res = 0 ;
            Stopwatch stopwatch = new Stopwatch();
            stopwatch.Start();
            if(summator!=null)
                res = summator.Sum(array);
            stopwatch.Stop();
            Console.WriteLine($"Result for {literal} is {res} obtained in {stopwatch.Elapsed}");
            return res;
        }

        void SetSumator(string lit)
        {
            switch (lit)
            {
                case "single" : summator = new OneThreadSummator(); break;
                case "parallel_interlocked": summator = new ParallelInterlockedSummator(); break;
                case "parallel_lock": summator = new ParallelLockSummator(); break;
                case "plinq": summator = new PLinqSummator(); break;
                case "linq": summator = new LinqSummator(); break;
                default:
                    break;
            }
        }

    }
}
