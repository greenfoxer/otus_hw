﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace home_work_15
{
    class ParallelInterlockedSummator : ISummator
    {
        long result = 0;
        object locker = new object();
        public long Sum(int[] array)
        {
            int sum=0;
            var response = Parallel.For(0, array.Length, (i) => { 
                Interlocked.Add(ref result, array[i]);
            });

            return result;
        }
    }
}
