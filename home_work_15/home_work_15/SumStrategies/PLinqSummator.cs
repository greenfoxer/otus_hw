﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Linq;

namespace home_work_15
{
    class PLinqSummator : ISummator
    {
        public long Sum(int[] array)
        {
            long res = 0;
            res = array.AsParallel().Sum();
            return res;
        }
    }
}
