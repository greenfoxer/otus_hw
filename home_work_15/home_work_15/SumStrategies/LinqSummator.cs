﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace home_work_15
{
    class LinqSummator : ISummator
    {
        public long Sum(int[] array)
        {
            long res = 0;
            res = array.Sum();
            return res;
        }
    }
}
