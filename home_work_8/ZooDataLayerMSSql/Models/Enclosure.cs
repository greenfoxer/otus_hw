﻿using System;
using System.Collections.Generic;

namespace ZooDataLayerMSSql.Models
{
    public partial class Enclosure
    {
        public Enclosure()
        {
            Animal = new HashSet<Animal>();
        }

        public long Id { get; set; }
        public string Name { get; set; }
        public string RoomNumber { get; set; }
        public string Description { get; set; }

        public virtual ICollection<Animal> Animal { get; set; }
    }
}
