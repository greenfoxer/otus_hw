﻿using System;
using System.Collections.Generic;

namespace ZooDataLayerMSSql.Models
{
    public partial class PersonalsEquipments
    {
        public long IdPerson { get; set; }
        public long IdEquipment { get; set; }
        public int Amount { get; set; }

        public virtual Equipment IdEquipmentNavigation { get; set; }
        public virtual Personal IdPersonNavigation { get; set; }
    }
}
