﻿using System;
using System.Collections.Generic;

namespace ZooDataLayerMSSql.Models
{
    public partial class Feeding
    {
        public long IdAnimal { get; set; }
        public TimeSpan Time { get; set; }
        public string Type { get; set; }
        public string Meal { get; set; }

        public virtual Animal IdAnimalNavigation { get; set; }
    }
}
