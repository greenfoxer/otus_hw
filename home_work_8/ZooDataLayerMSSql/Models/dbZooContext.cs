﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace ZooDataLayerMSSql.Models
{
    public partial class dbZooContext : DbContext
    {
        public dbZooContext()
        {
        }

        public dbZooContext(DbContextOptions<dbZooContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Animal> Animal { get; set; }
        public virtual DbSet<AnimalsPersonals> AnimalsPersonals { get; set; }
        public virtual DbSet<Enclosure> Enclosure { get; set; }
        public virtual DbSet<Equipment> Equipment { get; set; }
        public virtual DbSet<Feeding> Feeding { get; set; }
        public virtual DbSet<Personal> Personal { get; set; }
        public virtual DbSet<PersonalsEquipments> PersonalsEquipments { get; set; }
        public virtual DbSet<Species> Species { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
                optionsBuilder.UseSqlServer("Server=.\\SQLExpress;Database=dbZoo;Trusted_Connection=True;");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Animal>(entity =>
            {
                entity.Property(e => e.DateBirth).HasColumnType("datetime");

                entity.Property(e => e.Description).IsUnicode(false);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.IdEnclosureNavigation)
                    .WithMany(p => p.Animal)
                    .HasForeignKey(d => d.IdEnclosure)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_tAnimal_tEnclosure");

                entity.HasOne(d => d.IdSpeciesNavigation)
                    .WithMany(p => p.Animal)
                    .HasForeignKey(d => d.IdSpecies)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_tAnimal_tSpecies");
            });

            modelBuilder.Entity<AnimalsPersonals>(entity =>
            {
                entity.HasNoKey();

                entity.HasOne(d => d.IdAnimalNavigation)
                    .WithMany(p => p.AnimalsPersonals)
                    .HasForeignKey(d => d.IdAnimal)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_tAnimalPersonal_tAnimal");

                entity.HasOne(d => d.IdPersonNavigation)
                    .WithMany(p => p.AnimalsPersonals)
                    .HasForeignKey(d => d.IdPerson)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_tAnimalPersonal_tPersonal");
            });

            modelBuilder.Entity<Enclosure>(entity =>
            {
                entity.Property(e => e.Description)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Name)
                    .HasMaxLength(10)
                    .IsFixedLength();

                entity.Property(e => e.RoomNumber)
                    .HasMaxLength(10)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Equipment>(entity =>
            {
                entity.Property(e => e.Description).IsUnicode(false);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Feeding>(entity =>
            {
                entity.HasNoKey();

                entity.Property(e => e.Meal)
                    .IsRequired()
                    .IsUnicode(false);

                entity.Property(e => e.Type)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.IdAnimalNavigation)
                    .WithMany(p => p.Feeding)
                    .HasForeignKey(d => d.IdAnimal)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_tFeeding_tAnimal");
            });

            modelBuilder.Entity<Personal>(entity =>
            {
                entity.Property(e => e.DateBirth).HasColumnType("datetime");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<PersonalsEquipments>(entity =>
            {
                entity.HasNoKey();

                entity.HasOne(d => d.IdEquipmentNavigation)
                    .WithMany(p => p.PersonalsEquipments)
                    .HasForeignKey(d => d.IdEquipment)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_tPersonEquipment_tEquipment");

                entity.HasOne(d => d.IdPersonNavigation)
                    .WithMany(p => p.PersonalsEquipments)
                    .HasForeignKey(d => d.IdPerson)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_tPersonEquipment_tPersonal");
            });

            modelBuilder.Entity<Species>(entity =>
            {
                entity.Property(e => e.Description).IsUnicode(false);

                entity.Property(e => e.Name)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.NameLat)
                    .HasMaxLength(255)
                    .IsUnicode(false);
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
