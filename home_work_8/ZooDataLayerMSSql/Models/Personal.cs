﻿using System;
using System.Collections.Generic;

namespace ZooDataLayerMSSql.Models
{
    public partial class Personal
    {
        public Personal()
        {
            AnimalsPersonals = new HashSet<AnimalsPersonals>();
            PersonalsEquipments = new HashSet<PersonalsEquipments>();
        }

        public long Id { get; set; }
        public string Name { get; set; }
        public int? Sex { get; set; }
        public DateTime? DateBirth { get; set; }

        public virtual ICollection<AnimalsPersonals> AnimalsPersonals { get; set; }
        public virtual ICollection<PersonalsEquipments> PersonalsEquipments { get; set; }
    }
}
