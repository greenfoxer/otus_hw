﻿using System;
using System.Collections.Generic;

namespace ZooDataLayerMSSql.Models
{
    public partial class Animal
    {
        public Animal()
        {
            AnimalsPersonals = new HashSet<AnimalsPersonals>();
            Feeding = new HashSet<Feeding>();
        }

        public long Id { get; set; }
        public long IdSpecies { get; set; }
        public string Name { get; set; }
        public int? Sex { get; set; }
        public DateTime DateBirth { get; set; }
        public string Description { get; set; }
        public long IdEnclosure { get; set; }

        public virtual Enclosure IdEnclosureNavigation { get; set; }
        public virtual Species IdSpeciesNavigation { get; set; }
        public virtual ICollection<AnimalsPersonals> AnimalsPersonals { get; set; }
        public virtual ICollection<Feeding> Feeding { get; set; }
    }
}
