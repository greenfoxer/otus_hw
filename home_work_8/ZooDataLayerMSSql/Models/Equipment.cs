﻿using System;
using System.Collections.Generic;

namespace ZooDataLayerMSSql.Models
{
    public partial class Equipment
    {
        public Equipment()
        {
            PersonalsEquipments = new HashSet<PersonalsEquipments>();
        }

        public long Id { get; set; }
        public string Name { get; set; }
        public int? Amount { get; set; }
        public string Description { get; set; }

        public virtual ICollection<PersonalsEquipments> PersonalsEquipments { get; set; }
    }
}
