﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using CommonLib;

namespace ZooData
{
    public partial class dbZooContext : DbContext, IMiddleDbContext
    {
        public dbZooContext()
        {
            //Database.EnsureCreated();
        }

        public dbZooContext(DbContextOptions<dbZooContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Animal> Animal { get; set; }
        public virtual DbSet<AnimalsPersonals> AnimalsPersonals { get; set; }
        public virtual DbSet<Enclosure> Enclosure { get; set; }
        public virtual DbSet<Equipment> Equipment { get; set; }
        public virtual DbSet<Feeding> Feeding { get; set; }
        public virtual DbSet<Personal> Personal { get; set; }
        public virtual DbSet<PersonalsEquipments> PersonalsEquipments { get; set; }
        public virtual DbSet<Species> Species { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
                optionsBuilder.UseSqlite("Filename = ./database.db");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Animal>(entity =>
            {
                entity.Property(e => e.DateBirth).HasColumnType("datetime");

                entity.Property(e => e.Description).IsUnicode(false);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.IdEnclosureNavigation)
                    .WithMany(p => p.Animals)
                    .HasForeignKey(d => d.IdEnclosure)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_tAnimal_tEnclosure");

                entity.HasOne(d => d.IdSpeciesNavigation)
                    .WithMany(p => p.Animal)
                    .HasForeignKey(d => d.IdSpecies)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_tAnimal_tSpecies");
            });

            modelBuilder.Entity<Enclosure>(entity =>
            {
                entity.Property(e => e.Description)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Name)
                    .HasMaxLength(10)
                    .IsFixedLength();

                entity.Property(e => e.RoomNumber)
                    .HasMaxLength(10)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Equipment>(entity =>
            {
                entity.Property(e => e.Description).IsUnicode(false);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Feeding>(entity =>
            {
                entity.Property(e => e.Meal)
                    .IsRequired()
                    .IsUnicode(false);

                entity.Property(e => e.Type)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.IdAnimalNavigation)
                    .WithMany(p => p.Feedings)
                    .HasForeignKey(d => d.IdAnimal)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_tFeeding_tAnimal");
            });

            modelBuilder.Entity<Personal>(entity =>
            {
                entity.Property(e => e.DateBirth).HasColumnType("datetime");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Species>(entity =>
            {
                entity.Property(e => e.Description).IsUnicode(false);

                entity.Property(e => e.Name)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.NameLat)
                    .HasMaxLength(255)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<PersonalsEquipments>(entity =>
            {
                entity.HasKey(pe => new { pe.IdPerson, pe.IdEquipment });

                entity.HasOne(d => d.Equipment)
                    .WithMany(p => p.Personals)
                    .HasForeignKey(d => d.IdEquipment)
                    .OnDelete(DeleteBehavior.ClientSetNull);

                entity.HasOne(d => d.Person)
                    .WithMany(p => p.Equipments)
                    .HasForeignKey(d => d.IdPerson)
                    .OnDelete(DeleteBehavior.ClientSetNull);
            });
            modelBuilder.Entity<AnimalsPersonals>(entity =>
            {
                entity.HasKey(ap => new { ap.IdAnimal, ap.IdPerson });

                entity.HasOne(d => d.IdAnimalNavigation)
                    .WithMany(p => p.Personals)
                    .HasForeignKey(d => d.IdAnimal)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_tAnimalPersonal_tAnimal");

                entity.HasOne(d => d.IdPersonNavigation)
                    .WithMany(p => p.Animals)
                    .HasForeignKey(d => d.IdPerson)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_tAnimalPersonal_tPersonal");
            });

            modelBuilder.Seed();

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);

        void IMiddleDbContext.SaveChanges()
        {
            this.SaveChanges();
        }
    }
}
