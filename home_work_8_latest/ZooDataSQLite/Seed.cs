﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using CommonLib;

namespace ZooData
{
    public static class ModelBuilderExtensions
    {
        public static void Seed(this ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Species>().HasData(InitialData.Species);
            modelBuilder.Entity<Personal>().HasData(InitialData.Personals);
            modelBuilder.Entity<Equipment>().HasData(InitialData.Equipment);
            modelBuilder.Entity<Enclosure>().HasData(InitialData.Enclosures);
            modelBuilder.Entity<Animal>().HasData(InitialData.Animals);
            modelBuilder.Entity<Feeding>().HasData(InitialData.Feedings);
        }
    }
}
