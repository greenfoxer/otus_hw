﻿using System;
using CommonLib;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using DataAccessLayer;
using ZooData;

namespace home_work_8_latest
{
    class Program
    {
        static void Main(string[] args)
        {
            IUnitOfWork uow = new UnitOfWork();

            var s1 = uow.GetRepository<Animal>().Get(includeProperties: "Personals,Feedings,IdSpeciesNavigation").FirstOrDefault();
            var s2 = uow.GetRepository<Personal>().GetById(1L);

            s1.SetResponsiblePersonal(s2);
            uow.Commit();

            var feed = new Feeding() { Meal = "Deer", Time = TimeSpan.Parse("10:30:00"), Type = "Meat" };
            s1.InsertFeeding(feed);
            uow.Commit();

            Console.WriteLine("Hello World!");
        }
    }
}
