﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using CommonLib;
using ZooData;

namespace DataAccessLayer
{
    public class UnitOfWork : IUnitOfWork
    {
        IMiddleDbContext _context;
        private Hashtable _repositories;

        public UnitOfWork(IMiddleDbContext context)
        {
            _context = context;
        }

        public UnitOfWork()
        {
            _context = new dbZooContext();
        }

        public void Commit()
        {
            _context.SaveChanges();
        }

        public IRepository<T> GetRepository<T>() where T: class, IBaseEntity
        {
            if (_repositories == null)
                _repositories = new Hashtable();

            var type = typeof(T).Name;

            if (_repositories.ContainsKey(type)) return (BaseRepository<T>)_repositories[type];

            var repositoryType = typeof(BaseRepository<>);
            var repositoryInstance = Activator.CreateInstance(repositoryType.MakeGenericType(typeof(T)), _context);

            _repositories.Add(type, repositoryInstance);

            return (BaseRepository<T>)_repositories[type];
        }
    }
}
