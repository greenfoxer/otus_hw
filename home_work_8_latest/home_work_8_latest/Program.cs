﻿using System;
using CommonLib;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using DataAccessLayer;
using ZooData;

namespace home_work_8_latest
{
    class Program
    {
        static void Main(string[] args)
        {
            IUnitOfWork uow = new UnitOfWork();

            var abstractAnimal = uow.GetRepository<Animal>().Get(includeProperties: "Personals,Feedings,IdSpeciesNavigation").FirstOrDefault();
            var administrator = uow.GetRepository<Personal>().GetById(1L,"Equipments");

            abstractAnimal.SetResponsiblePersonal(administrator);
            uow.Commit();

            var feed = new Feeding() { Meal = "Deer", Time = TimeSpan.Parse("10:30:00"), Type = "Meat" };
            abstractAnimal.InsertFeeding(feed);
            uow.Commit();

            var showel = uow.GetRepository<Equipment>().GetById(1L);
            administrator.TakeEquipment(showel);
            uow.Commit();

            var newSpecies = new Species() { Description = "Russian predator", Name = "Bear", NameLat = "Bear" };
            uow.GetRepository<Species>().Insert(newSpecies);
            uow.Commit();

            Console.WriteLine("Hello World!");
        }
    }
}
