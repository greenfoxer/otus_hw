﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CommonLib
{
    public static class InitialData
    {
       public static Species[] Species = { 
                                new Species() { Id = 1, Description = "Abstract Animal", Name = "Abstract", NameLat = "Abstractious" } ,
                                new Species() { Id = 2,Description = "Tigers roarrr!", Name = "Tiger", NameLat = "Tiger" },
                                new Species() { Id = 3,Description = "Fox diring-diring", Name = "Fox", NameLat = "Foxes" }
                                };
        public static Enclosure[] Enclosures = {
                                new Enclosure() { Id = 1, Name= "General", RoomNumber="0", Description= "Common field for animals." },
                                new Enclosure() { Id = 2, Name= "Cold desert", RoomNumber="T-1", Description= "Tundra. Freeze cold." },
                                new Enclosure() { Id = 3, Name= "Savannah", RoomNumber="S-1", Description= "Savannah. Sun and warm" }
                                };
        public static Personal[] Personals = {
                                new Personal() { Id = 1, Name = "Administrator", Sex=0 },
                                new Personal() { Id = 2,Name = "Eric", Sex=1 },
                                new Personal() { Id = 3,Name = "Samantha", Sex=0 }};
        public static Equipment[] Equipment = { new Equipment() {Id = 1, Name ="Showel", Amount = 5, Description = "Cleaning showel." },
                                new Equipment() { Id = 2,Name ="Glove", Amount = 50, Description = "Protect your hands!" },
                                new Equipment() { Id = 3,Name ="Rake", Amount = 5, Description = "Clean after this elephant!" }
                                };
        public static Animal[] Animals = {
                                new Animal() { Id = 1, IdSpecies=1, IdEnclosure = 1, DateBirth = DateTime.Now, Description = "First animal", Name = "Exterminator", Sex = 1 } };
        public static Feeding[] Feedings = {
                                new Feeding() {Id = 1, IdAnimal = 1, Meal = "Grass", Type = "Vegetable"},
                                new Feeding() { Id = 2, IdAnimal = 1, Meal = "Rabbit", Type = "Meat"},
                                new Feeding() { Id = 3, IdAnimal = 1, Meal = "Half of Deer", Type = "Meat" }
        };
        public static void Init()
        { 
            
        }
    }
}
