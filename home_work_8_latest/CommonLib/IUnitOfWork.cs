﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CommonLib
{
    public interface IUnitOfWork
    {
        void Commit();
        IRepository<T> GetRepository<T>() where T : class, IBaseEntity;
    }
}
