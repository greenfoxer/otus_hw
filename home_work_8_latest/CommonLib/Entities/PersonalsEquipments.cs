﻿using System;
using System.Collections.Generic;

namespace CommonLib
{
    public partial class PersonalsEquipments
    {
        public long IdPerson { get; set; }
        public long IdEquipment { get; set; }
        public int Amount { get; set; }

        public virtual Equipment Equipment { get; set; }
        public virtual Personal Person { get; set; }
    }
}
