﻿using System;
using System.Collections.Generic;

namespace CommonLib
{
    public partial class Enclosure : IBaseEntity
    {
        public Enclosure()
        {
            Animals = new HashSet<Animal>();
        }

        public long Id { get; set; }
        public string Name { get; set; }
        public string RoomNumber { get; set; }
        public string Description { get; set; }

        public virtual ICollection<Animal> Animals { get; set; }
    }
}
