﻿using System;
using System.Collections.Generic;

namespace CommonLib
{
    public partial class Species : IBaseEntity
    {
        public Species()
        {
            Animal = new HashSet<Animal>();
        }

        public long Id { get; set; }
        public string Name { get; set; }
        public string NameLat { get; set; }
        public string Description { get; set; }

        public virtual ICollection<Animal> Animal { get; set; }
    }
}
