﻿using System;
using System.Collections.Generic;

namespace CommonLib
{
    public partial class AnimalsPersonals
    {
        public long IdAnimal { get; set; }
        public long IdPerson { get; set; }

        public virtual Animal IdAnimalNavigation { get; set; }
        public virtual Personal IdPersonNavigation { get; set; }
    }
}
