﻿using System;
using System.Collections.Generic;

namespace CommonLib
{
    public partial class Equipment : IBaseEntity
    {
        public Equipment()
        {
            Personals = new HashSet<PersonalsEquipments>();
        }

        public long Id { get; set; }
        public string Name { get; set; }
        public int? Amount { get; set; }
        public string Description { get; set; }

        public virtual ICollection<PersonalsEquipments> Personals { get; set; }
    }
}
