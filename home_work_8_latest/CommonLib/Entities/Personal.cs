﻿using System;
using System.Collections.Generic;
using System.Linq;
namespace CommonLib
{
    public partial class Personal : IBaseEntity
    {
        public Personal()
        {
            Animals = new HashSet<AnimalsPersonals>();
            Equipments = new HashSet<PersonalsEquipments>();
        }

        public long Id { get; set; }
        public string Name { get; set; }
        public int? Sex { get; set; }
        public DateTime? DateBirth { get; set; }

        public virtual ICollection<AnimalsPersonals> Animals { get; set; }
        public virtual ICollection<PersonalsEquipments> Equipments { get; set; }

        public void TakeEquipment(Equipment equipment)
        {
            var eq = Equipments.FirstOrDefault(p => p.IdEquipment == equipment.Id && p.IdPerson == this.Id);
            if (eq is null)
                Equipments.Add(new PersonalsEquipments() { Equipment = equipment, Person = this, Amount = 1 });
            else
                eq.Amount++;
        }
    }
}
