﻿using System;
using System.Collections.Generic;

namespace CommonLib
{
    public partial class Feeding : IBaseEntity
    {
        public long Id { get; set; }

        public long? IdAnimal { get; set; }
        public TimeSpan Time { get; set; }
        public string Type { get; set; }
        public string Meal { get; set; }

        public virtual Animal IdAnimalNavigation { get; set; }
    }
}
