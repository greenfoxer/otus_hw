﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace CommonLib
{
    public partial class Animal : IBaseEntity
    {

        public Animal()
        {
            Feedings = new HashSet<Feeding>();
            Personals = new HashSet<AnimalsPersonals>();
        }

        public long Id { get; set; }
        public long IdSpecies { get; set; }
        public string Name { get; set; }
        public int? Sex { get; set; }
        public DateTime DateBirth { get; set; }
        public string Description { get; set; }
        public long IdEnclosure { get; set; }

        public virtual Enclosure IdEnclosureNavigation { get; set; }
        public virtual Species IdSpeciesNavigation { get; set; }
        public virtual ICollection<AnimalsPersonals> Personals { get; set; }
        public virtual ICollection<Feeding> Feedings { get; set; }

        //End of auto-generated region

        public void SetSex(int sex)
        {
            this.Sex = sex;
        }

        public void SetResponsiblePersonal(Personal person)
        {
            if(Personals.FirstOrDefault(p=>p.IdPerson == person.Id) == null)
                Personals.Add(new AnimalsPersonals() { IdAnimalNavigation = this, IdPersonNavigation = person });
        }
        public void InsertFeeding(Feeding feed)
        {
            feed.IdAnimal = this.Id;
            Feedings.Add(feed);
        }
    }
}
