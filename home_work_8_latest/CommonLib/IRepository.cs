﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Linq.Expressions;

namespace CommonLib
{
    public interface IRepository<TEntity> where TEntity : class, IBaseEntity
    {
        abstract void Delete(TEntity entity);
        abstract void Delete(object id);
        abstract IEnumerable<TEntity> Get(
            Expression<Func<TEntity, bool>> filter = null,
            Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null,
            string includeProperties = "");
        abstract TEntity GetById(object id, string include="");
        abstract IEnumerable<TEntity> GetWithRawSql(string query, params object[] parameters);
        abstract void Insert(TEntity entity);
        abstract void Update(TEntity entity);
    }
}
