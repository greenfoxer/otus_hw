﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using CommonLib;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;


namespace ZooData
{
    public class BaseRepository<TEntity> : IRepository<TEntity> where TEntity : class, IBaseEntity
    {
        internal DbContext Context;

        public BaseRepository(IMiddleDbContext context)
        {
            Context = context as DbContext;
        }

        public void Delete(TEntity entity)
        {
            if (Context.Entry(entity).State == EntityState.Detached)
            {
                Context.Set<TEntity>().Attach(entity);
            }
            Context.Set<TEntity>().Remove(entity);
        }

        public void Delete(object id)
        {
            TEntity entity = Context.Set<TEntity>().Find(id);
            Delete(entity);
        }

        public IEnumerable<TEntity> Get(Expression<Func<TEntity, bool>> filter = null, Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null, string includeProperties = "")
        {
            IQueryable<TEntity> query = Context.Set<TEntity>();
            if (filter != null)
                query.Where(filter);
            if (includeProperties != null)
            {
                foreach (var includeProperty in includeProperties.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries))
                    query = query.Include(includeProperty);
            }
            if (orderBy != null)
                return orderBy(query).ToList();
            else
                return query.ToList();
        }

        public TEntity GetById(object id, string includeProperties = "")
        {
            if (string.IsNullOrEmpty(includeProperties))
                return Context.Set<TEntity>().Find(id);
            else
            {
                IQueryable<TEntity> query = Context.Set<TEntity>();
                foreach (var includeProperty in includeProperties.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries))
                    query = query.Include(includeProperty);
                return query.FirstOrDefault(p=> p.Id == (long)id);
            }
        }

        public IEnumerable<TEntity> GetWithRawSql(string query, params object[] parameters)
        {
            return Context.Set<TEntity>().FromSqlRaw<TEntity>(query, parameters).ToList();
        }

        public void Insert(TEntity entity)
        {
            Context.Set<TEntity>().Add(entity);
            Context.SaveChanges();
        }

        public void Update(TEntity entity)
        {
            Context.Set<TEntity>().Attach(entity);
            Context.Entry(entity).State = EntityState.Modified;
        }
    }
}
