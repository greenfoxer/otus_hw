﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace ZooDataMSSql.Migrations
{
    public partial class second : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Enclosure",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(fixedLength: true, maxLength: 50, nullable: true),
                    RoomNumber = table.Column<string>(unicode: false, maxLength: 10, nullable: true),
                    Description = table.Column<string>(unicode: false, maxLength: 255, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Enclosure", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Equipment",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(unicode: false, maxLength: 50, nullable: false),
                    Amount = table.Column<int>(nullable: true),
                    Description = table.Column<string>(unicode: false, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Equipment", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Personal",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(unicode: false, maxLength: 100, nullable: false),
                    Sex = table.Column<int>(nullable: true),
                    DateBirth = table.Column<DateTime>(type: "datetime", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Personal", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Species",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(unicode: false, maxLength: 255, nullable: true),
                    NameLat = table.Column<string>(unicode: false, maxLength: 255, nullable: true),
                    Description = table.Column<string>(unicode: false, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Species", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "PersonalsEquipments",
                columns: table => new
                {
                    IdPerson = table.Column<long>(nullable: false),
                    IdEquipment = table.Column<long>(nullable: false),
                    Amount = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PersonalsEquipments", x => new { x.IdPerson, x.IdEquipment });
                    table.ForeignKey(
                        name: "FK_PersonalsEquipments_Equipment_IdEquipment",
                        column: x => x.IdEquipment,
                        principalTable: "Equipment",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_PersonalsEquipments_Personal_IdPerson",
                        column: x => x.IdPerson,
                        principalTable: "Personal",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Animal",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    IdSpecies = table.Column<long>(nullable: false),
                    Name = table.Column<string>(unicode: false, maxLength: 50, nullable: false),
                    Sex = table.Column<int>(nullable: true),
                    DateBirth = table.Column<DateTime>(type: "datetime", nullable: false),
                    Description = table.Column<string>(unicode: false, nullable: true),
                    IdEnclosure = table.Column<long>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Animal", x => x.Id);
                    table.ForeignKey(
                        name: "FK_tAnimal_tEnclosure",
                        column: x => x.IdEnclosure,
                        principalTable: "Enclosure",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_tAnimal_tSpecies",
                        column: x => x.IdSpecies,
                        principalTable: "Species",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "AnimalsPersonals",
                columns: table => new
                {
                    IdAnimal = table.Column<long>(nullable: false),
                    IdPerson = table.Column<long>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AnimalsPersonals", x => new { x.IdAnimal, x.IdPerson });
                    table.ForeignKey(
                        name: "FK_tAnimalPersonal_tAnimal",
                        column: x => x.IdAnimal,
                        principalTable: "Animal",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_tAnimalPersonal_tPersonal",
                        column: x => x.IdPerson,
                        principalTable: "Personal",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Feeding",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    IdAnimal = table.Column<long>(nullable: false),
                    Time = table.Column<TimeSpan>(nullable: false),
                    Type = table.Column<string>(unicode: false, maxLength: 50, nullable: true),
                    Meal = table.Column<string>(unicode: false, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Feeding", x => x.Id);
                    table.ForeignKey(
                        name: "FK_tFeeding_tAnimal",
                        column: x => x.IdAnimal,
                        principalTable: "Animal",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.InsertData(
                table: "Equipment",
                columns: new[] { "Id", "Amount", "Description", "Name" },
                values: new object[,]
                {
                    { 1L, 5, "Cleaning showel.", "Showel" },
                    { 2L, 50, "Protect your hands!", "Glove" },
                    { 3L, 5, "Clean after this elephant!", "Rake" }
                });

            migrationBuilder.InsertData(
                table: "Enclosure",
                columns: new[] { "Id", "Description", "Name", "RoomNumber" },
                values: new object[,]
                {
                    { 1L, "Common field for animals.", "General", "0" },
                    { 2L, "Tundra. Freeze cold.", "Cold desert", "T-1" },
                    { 3L, "Savannah. Sun and warm", "Savannah", "S-1" }
                });

            migrationBuilder.InsertData(
                table: "Personal",
                columns: new[] { "Id", "DateBirth", "Name", "Sex" },
                values: new object[,]
                {
                    { 1L, null, "Administrator", 0 },
                    { 2L, null, "Eric", 1 },
                    { 3L, null, "Samantha", 0 }
                });

            migrationBuilder.InsertData(
                table: "Species",
                columns: new[] { "Id", "Description", "Name", "NameLat" },
                values: new object[,]
                {
                    { 1L, "Abstract Animal", "Abstract", "Abstractious" },
                    { 2L, "Tigers roarrr!", "Tiger", "Tiger" },
                    { 3L, "Fox diring-diring", "Fox", "Foxes" }
                });

            migrationBuilder.InsertData(
                table: "Animal",
                columns: new[] { "Id", "DateBirth", "Description", "IdEnclosure", "IdSpecies", "Name", "Sex" },
                values: new object[] { 1L, new DateTime(2020, 3, 31, 12, 44, 32, 781, DateTimeKind.Local).AddTicks(6553), "First animal", 1L, 1L, "Exterminator", 1 });

            migrationBuilder.InsertData(
                table: "Feeding",
                columns: new[] { "Id", "IdAnimal", "Meal", "Time", "Type" },
                values: new object[,] {
                { 2L, 1L, "Rabbit", new TimeSpan(0, 0, 0, 0, 0), "Meat" },
                    { 3L, 1L, "Half of Deer", new TimeSpan(0, 0, 0, 0, 0), "Meat" },
                { 1L, 1L, "Grass", new TimeSpan(0, 0, 0, 0, 0), "Vegetable" }
                });

            migrationBuilder.CreateIndex(
                name: "IX_Animal_IdEnclosure",
                table: "Animal",
                column: "IdEnclosure");

            migrationBuilder.CreateIndex(
                name: "IX_Animal_IdSpecies",
                table: "Animal",
                column: "IdSpecies");

            migrationBuilder.CreateIndex(
                name: "IX_AnimalsPersonals_IdPerson",
                table: "AnimalsPersonals",
                column: "IdPerson");

            migrationBuilder.CreateIndex(
                name: "IX_Feeding_IdAnimal",
                table: "Feeding",
                column: "IdAnimal");

            migrationBuilder.CreateIndex(
                name: "IX_PersonalsEquipments_IdEquipment",
                table: "PersonalsEquipments",
                column: "IdEquipment");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "AnimalsPersonals");

            migrationBuilder.DropTable(
                name: "Feeding");

            migrationBuilder.DropTable(
                name: "PersonalsEquipments");

            migrationBuilder.DropTable(
                name: "Animal");

            migrationBuilder.DropTable(
                name: "Equipment");

            migrationBuilder.DropTable(
                name: "Personal");

            migrationBuilder.DropTable(
                name: "Enclosure");

            migrationBuilder.DropTable(
                name: "Species");
        }
    }
}
