﻿using System;

namespace home_work_11
{
    class Program
    {
        static void Main(string[] args)
        {
            // Структура классов предельно простая: Человек(Person) |-> Преподаватель(Teacher) -> Профессор(Professor)
            //                                                      |-> Студент(Student)
            // Так же для демонстрации введен класс Курса(Course), который содержится в классе Student
            // В классе Student так же рассмотренно предоставляемое C# неглубокое копирование для рассмотрения разности между подходами.

            Teacher t = new Teacher("John", "Doe", 50, "unknown", "Mathematics"); 
            Teacher t1 = (Teacher)t.DeepClone();                                    // Клонирование Teacher - наследника класса Person

            Person p = new Professor("John","Doe", 50, "unknown","Mathematics","PhD");
            Professor p1 = (Professor)p.DeepClone();                                // Клонирование Professor - наследника класса Teacher

            (p1 as Professor).dosomething();        // Check

            Person p3 = new Person("John", "Doe", 450, "unknown");
            Person p4 = (Person)p3.DeepClone();                                     // Клонирование Person
            p3.Age = 100;
            Console.WriteLine(p4.Age);              // Check
            
            Course c1 = new Course("Physics", 200);
            Student s1 = new Student("Alex", "Smith", 19, "summerfield", "Physics-1",c1);
            Student s2 = (Student)s1.DeepClone();                                   // Клонирование Student - наследника класса Person
            Student s3 = s1.ShallowCopy();                                          // Клонирование Student - наследника класса Person, методами C#
            c1.Title = "Nuclear Physics";
            Console.WriteLine(s1.MainCourse.Title);
            Console.WriteLine(s2.MainCourse.Title);
            Console.WriteLine(s3.MainCourse.Title);

            Person p5 = new Person("Lev", "Fatty", 140, "Moscow");                  // Клонирование Person реализованным методом интерфейса ICloneable
            Person p6 = (Person)p5.Clone();

            var t6 = p5.Clone();
            Console.WriteLine(p6.GetType());
            Console.WriteLine(t6.GetType());

        }
    }
}
