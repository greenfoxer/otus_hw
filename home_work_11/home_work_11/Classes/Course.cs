﻿using System;
using System.Collections.Generic;
using System.Text;

namespace home_work_11
{
    public class Course : IMyCloneable
    {
        public string Title { get; set; }
        private int Duration;
        public Course(string title, int duration)
        {
            this.Title = title;
            this.Duration = duration;
        }

        protected Course() { }

        public IMyCloneable DeepClone()
        {
            var result = (Course)CreateInstanceForClone();
            result.Duration = this.Duration;
            result.Title = this.Title;
            return result;
        }

        protected virtual Course CreateInstanceForClone()
        {
            return new Course();
        }
    }
}
