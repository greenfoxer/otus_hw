﻿using System;
using System.Collections.Generic;
using System.Text;

namespace home_work_11
{
    public interface IMyCloneable
    {
        IMyCloneable DeepClone();
    }
}
