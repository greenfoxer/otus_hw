﻿using System;
using System.Collections.Generic;
using System.Text;

namespace home_work_11
{
    public class Student : Person , IMyCloneable
    {
        public Course MainCourse { get; set; }
        public string Group { get; set; }

        public Student(string fname, string sname, int age, string address, string group, Course course)
            : base(fname, sname, age, address)
        {
            this.Group = group;
            this.MainCourse = course;
        }

        protected Student() { }

        public override IMyCloneable DeepClone()
        {
            var result = (Student)base.DeepClone();
            result.MainCourse = (Course)MainCourse.DeepClone();
            result.Group = this.Group;
            return result;
        }

        public Student ShallowCopy()
        {
            return (Student)this.MemberwiseClone();
        }

        protected override Person CreateInstanceForClone()
        {
            return new Student();
        }

    }
}
