﻿using System;
using System.Collections.Generic;
using System.Text;

namespace home_work_11
{
    public class Person : IMyCloneable, ICloneable
    {
        public string FirstName { get; set; }
        public string SecondName { get; set; }
        protected internal int Age { get; set; }
        private string Address { get; set; }

        public Person(string firstName, string secondName, int age, string address)
        {
            FirstName = firstName;
            SecondName = secondName;
            Age = age;
            Address = address;
        }
        protected Person() { }

        // Три метода для тестирования private & protected свойств и полей снаружи - из проекта для теста
        public int GetAge()
        {
            return this.Age;
        }
        public void SetAge(int n)
        {
            this.Age = n;
        }
        public string GetAddress()
        {
            return this.Address;
        }

        public virtual IMyCloneable DeepClone()
        {
            var result = CreateInstanceForClone();
            result.FirstName = this.FirstName;
            result.SecondName = this.SecondName;
            result.Age = this.Age;
            result.Address = this.Address;
            return result;
        }
        protected virtual Person CreateInstanceForClone()
        {
            return new Person();
        }

        public virtual object Clone()
        {
            return DeepClone();
        }
    }
}
