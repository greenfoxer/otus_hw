﻿using System;
using System.Collections.Generic;
using System.Text;

namespace home_work_11
{
    public class Teacher : Person, IMyCloneable
    {
        protected internal string Subject { get; set; } // прдемет, который преподает учитель

        public Teacher(string fname, string sname, int age, string address, string subject) //класс учителя
            :base(fname,sname,age,address)
        {
            Subject = subject;
        }

        protected Teacher() { }

        protected override Person CreateInstanceForClone() // производим апкаст нового объекта Teacher до Person
        {
            return new Teacher();
        }

        public override IMyCloneable DeepClone()
        {
            var result = (Teacher)base.DeepClone();
            result.Subject = this.Subject;               //добавляем уникальный для данного класса элемент Subject
            return result;
        }

        public override object Clone()
        {
            return DeepClone();
        }
    }
}
