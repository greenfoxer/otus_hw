﻿using System;
using System.Collections.Generic;
using System.Text;

namespace home_work_11
{
    public class Professor: Teacher , IMyCloneable, ICloneable
    {
        protected string Rang { get; set; }
        public Professor(string fname, string sname, int age, string address, string position, string rang)
            : base(fname, sname, age, address,position)
        {
            Rang = rang;
        }

        protected Professor() { }

        public void dosomething() // test method
        {
            Age = 9;
        }

        public override IMyCloneable DeepClone()
        {
            var result = (Professor)base.DeepClone();
            result.Rang = this.Rang;
            return result;
        }

        protected override Person CreateInstanceForClone()
        {
            return new Professor();
        }

        public override object Clone()
        {
            return DeepClone();
        }
    }
}
