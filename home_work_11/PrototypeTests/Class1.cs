﻿using System;
using NUnit.Framework;
using home_work_11;

namespace PrototypeTests
{
    [TestFixture]
    public class Class1
    {
        /// <summary>
        /// Тесты, направленные на проверку, что клонированные объекты совпадают по данным с исходными, и при изменении исходных - не меняются.
        /// </summary>
        [Test]
        public void CheckPersonClone()
        {

            Person p1 = new Person("John", "Doe", 450, "unknown");
            Person p2 = (Person)p1.DeepClone();                                     // Клонирование Person
            Assert.AreEqual(p1.GetAge(), p2.GetAge(), "Защищенные свойства копированных объектов не совпадают!");
            Assert.AreEqual(p1.GetAddress(), p2.GetAddress(), "Приватные свойства копированных объектов не совпадают!");
            p2.SetAge(100);
            Assert.AreNotEqual(p1.GetAge(), p2.GetAge(), "Защищенное свойство изменилось у обоих копированных объектов!");

        }
        [Test]
        public void CheckTeacherClone()
        {
            Teacher t1 = new Teacher("John", "Doe", 50, "unknown", "Mathematics");
            Teacher t2 = (Teacher)t1.DeepClone();
            Assert.AreEqual(t1.GetAge(), t2.GetAge(), "Защищенные свойства копированных объектов не совпадают!");
            Assert.AreEqual(t1.GetAddress(), t2.GetAddress(), "Приватные свойства копированных объектов не совпадают!");
            t2.SetAge(100);
            Assert.AreNotEqual(t1.GetAge(), t2.GetAge(), "Защищенное свойство изменилось у обоих копированных объектов!");
        }
        [Test]
        public void CheckProfessorClone()
        {
            Person p1 = new Professor("John", "Doe", 50, "unknown", "Mathematics", "PhD");
            Professor p2 = (Professor)p1.DeepClone();
            Assert.AreEqual(p1.GetAge(), p2.GetAge(), "Защищенные свойства копированных объектов не совпадают!");
            Assert.AreEqual(p1.GetAddress(), p2.GetAddress(), "Приватные свойства копированных объектов не совпадают!");
            p2.SetAge(100);
            Assert.AreNotEqual(p1.GetAge(), p2.GetAge(), "Защищенное свойство изменилось у обоих копированных объектов!");
        }
        [Test]
        public void CheckStudentDeepClone()
        {
            Course c1 = new Course("Physics", 200);
            Student s1 = new Student("Alex", "Smith", 19, "summerfield", "Physics-1", c1);
            Student s2 = (Student)s1.DeepClone();
            Assert.AreEqual(s1.GetAge(), s2.GetAge(), "Защищенные свойства копированных объектов не совпадают!");
            Assert.AreEqual(s1.MainCourse.Title, s2.MainCourse.Title, "Ссылочные компоненты копированных объектов не совпадают!");
            s2.MainCourse.Title = "Literature";
            Assert.AreNotEqual(s1.MainCourse.Title, s2.MainCourse.Title, "Ссылочные компоненты копированных объектов остались идентичны при изменении одного объекта!");
        }
        [Test]
        public void CheckStudentShallowClone()
        {
            Course c1 = new Course("Physics", 200);
            Student s1 = new Student("Alex", "Smith", 19, "summerfield", "Physics-1", c1);
            Student s2 = (Student)s1.ShallowCopy();
            Assert.AreEqual(s1.GetAge(), s2.GetAge(), "Защищенные свойства копированных объектов не совпадают!");
            Assert.AreEqual(s1.MainCourse.Title, s2.MainCourse.Title, "Ссылочные компоненты копированных объектов не совпадают!");
            s2.MainCourse.Title = "Literature";
            Assert.AreEqual(s1.MainCourse.Title, s2.MainCourse.Title, "Ссылочные компоненты копированных объектов оказались разными при изменении одного объекта!");
        }
        /// <summary>
        /// Успешно пройденный тест говорит, что при реализации интерфейса методами, написанными для интерфейсв IMyCloneable
        /// мы получим тот же результат.
        /// </summary>
        [Test]
        public void CheckICloneable()
        {
            Course c1 = new Course("Physics", 200);
            Student s1 = new Student("Alex", "Smith", 19, "summerfield", "Physics-1", c1);
            Student s2 = (Student)s1.Clone();
            object s3 = s1.Clone();
            Assert.AreEqual(s1.GetAge(), s2.GetAge(), "Защищенные свойства копированных объектов не совпадают!");
            Assert.AreEqual(s1.MainCourse.Title, s2.MainCourse.Title, "Ссылочные компоненты копированных объектов не совпадают!");
            s2.MainCourse.Title = "Literature";
            Assert.AreNotEqual(s1.MainCourse.Title, s2.MainCourse.Title, "Ссылочные компоненты копированных объектов остались идентичны при изменении одного объекта!");
        }
    }
}
